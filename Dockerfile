FROM python:3
ENV PYTHONUNBUFFERED 1

RUN pip install pip-tools
RUN mkdir -p /code
WORKDIR /code
COPY requirements.in /code/
RUN pip-compile --output-file requirements.txt requirements.in
RUN pip install -r requirements.txt
COPY . /code/
