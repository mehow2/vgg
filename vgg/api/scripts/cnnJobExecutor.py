import numpy as np
from api.settings import IMG_ANALYSIS_PATH
from keras.applications.vgg19 import VGG19, preprocess_input
from keras.models import Model
from keras.preprocessing import image


class CnnJobExecutor(object):
    def __init__(self, layer):
        self.base_model = VGG19(weights='imagenet', include_top=False, pooling='avg')
        self.model = Model(inputs=self.base_model.input, outputs=self.base_model.get_layer(layer).output)
        self.model._make_predict_function()

    def execute(self):
        img = image.load_img(IMG_ANALYSIS_PATH, target_size=(224, 224))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        features = self.model.predict(x)
        return features
