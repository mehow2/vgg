from django.conf.urls import url
from vgg.api import views


urlpatterns = [
    url('vgg/', views.VggView.as_view())
]
