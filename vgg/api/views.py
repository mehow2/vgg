from rest_framework.response import Response
from rest_framework.views import APIView
from vgg.api.scripts import executor
from vgg.api.scripts.imgDownloader import ImgDownloader
from vgg.api.serializers import UrlSerializer


class VggView(APIView):
    serializer_class = UrlSerializer

    def get(self, request):
        downloader = ImgDownloader()
        serializer = self.serializer_class(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        url = serializer.validated_data['url']
        downloader.download(url)
        features = executor.execute()
        return Response(features)
