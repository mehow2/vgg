## Requirements
- `docker==18.06.3-ce`
- `docker-compose==1.23.2`

## Setup
- `docker-compose run -p 8000:8000 --rm -d -e LAYER=layer api`

e.g.
- `docker-compose run -p 8000:8000 --rm -d -e LAYER="block4_pool" api`

You can run multiple instances of container for layers on one host by specifing different ports e.g.

- `docker-compose run -p 8001:8000 --rm -d -e LAYER="block1_pool" api`
- `docker-compose run -p 8002:8000 --rm -d -e LAYER="block2_pool" api`


## Usage
Send HTTP GET to `0.0.0.0:8000/api/vgg/?url=img_url`
