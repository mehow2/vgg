import requests
from api.settings import IMG_ANALYSIS_PATH


class ImgDownloader():
    def download(self, imgUrl):
        img_data = requests.get(imgUrl).content
        with open(IMG_ANALYSIS_PATH, 'wb+') as handler:
            handler.write(img_data)
